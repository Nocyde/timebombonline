import React from "react";
import vide from "../images/time bomb/carte vide.png";
import cable from "../images/time bomb/cable.png";
import boom from "../images/time bomb/boom.png";
import verso from "../images/time bomb/face cachée.png";


function Rules (props) {
    return(
    <div id="body">
        <h1 id="time-bomb">Time Bomb</h1>
        <br/><h2 id="1-avant-la-partie-">1. Avant la partie</h2><br/>
<p>Pour lancer une partie, l&#39;un des joueurs doit créer un salon. Il doit ainsi simplement renseigner son pseudo et cliquer sur le bouton Créer un salon.
Il indique ensuite aux autres joueurs le numéro de son salon pour qu&#39;ils puissent le rejoindre. Ils doivent également renseigner leur pseudo mais aussi le numéro du salon qu&#39;ils souhaitent rejoindre avant de cliquer sur le bouton Rejoindre un salon.</p>
<br/><p>Un salon peut accueillir jusqu’à 8 joueurs maximum, et on ne peut lancer la partie que s’il y en a 4 minimum.
<br/>Aucun joueur ne peut rejoindre un salon quand une partie y est déjà en cours.</p>
<br/><h2 id="2-but-du-jeu">2. But du jeu</h2><br/>
<p>C&#39;est un jeu un tout petit peu manichéen. Il y a les gentils contre les méchants.</p><br/>
<p>Au début de la partie vous découvrez dans quel camp vous êtes mais ne connaissez pas le rôle des autres joueurs, à vous de tenter d&#39;identifier vos partenaires !</p><br/>
<p>Les gentils gagnent s&#39;ils désamorcent la bombe, les méchants gagnent si elle explose.</p>
<br/><h2 id="3-mise-en-place-">3. Mise en place</h2><br/>
<p>Deck cartes Rôle :</p>
<table>
<thead>
<tr>
<th>Nombre de joueurs</th>
<th>Gentils</th>
<th>Méchants</th>
</tr>
</thead>
<tbody>
<tr>
<td>4</td>
<td>3</td>
<td>2</td>
</tr>
<tr>
<td>5</td>
<td>3</td>
<td>2</td>
</tr>
<tr>
<td>6</td>
<td>4</td>
<td>2</td>
</tr>
<tr>
<td>7</td>
<td>5</td>
<td>3</td>
</tr>
<tr>
<td>8</td>
<td>5</td>
<td>3</td>
</tr>
</tbody>
</table>
<p>Distribuer une carte par joueur puis jeter ce qu’il reste (il y a une carte en trop dans les cas 4 et 7 joueurs)</p><br/>
<p>Deck cartes Câble :</p>
<table>
<thead>
<tr>
<th>Nombre de joueurs</th>
<th>Cartes vides</th>
<th>Câbles</th>
<th>BOOM</th>
</tr>
</thead>
<tbody>
<tr>
<td>4</td>
<td>15</td>
<td>4</td>
<td>1</td>
</tr>
<tr>
<td>5</td>
<td>19</td>
<td>5</td>
<td>1</td>
</tr>
<tr>
<td>6</td>
<td>23</td>
<td>6</td>
<td>1</td>
</tr>
<tr>
<td>7</td>
<td>27</td>
<td>7</td>
<td>1</td>
</tr>
<tr>
<td>8</td>
<td>31</td>
<td>8</td>
<td>1</td>
</tr>
</tbody>
</table>
<p>Il y a toujours exactement une carte BOOM, une carte câble par joueur et on complète par des cartes vides afin qu&#39;il y ait 5 cartes par joueur.</p><br/>
<p>Distribuer 5 cartes à chaque joueur.</p>
<p>Choisir un joueur aléatoirement qui aura la main en début de partie.</p><br/>
<p>Chaque joueur prend connaissance de ses cartes, il est en &quot;En attente&quot;. Chacun voit alors ses propres cartes mais pas celles des autres. Quand on est prêt, on peut appuyer sur le bouton READY. Nos cartes se mélangent alors avant d&#39;être retournées. Quand plus personne n&#39;est en attente, la partie commence. </p>
<br/><h2 id="rules4">4. Cœur de la partie</h2><br/>
<p>On enchaîne 4 manches.</p><br/>
<p>Déroulement d’une manche :</p><br/>
<ul>
<li>le joueur qui a la main peut cliquer sur n’importe quelle carte sauf l’une des siennes pour la retourner</li>
<li>la carte est dévoilée pour tout le monde</li>
<li>le joueur dont la carte vient d’être choisie a à présent la main et peut cliquer sur une carte</li>
<li>lorsqu’autant de cartes qu’il y a de joueurs ont été dévoilées, la manche est finie</li>
</ul>
<br/><h2 id="5-fin-de-partie">5. Fin de partie</h2><br/>
<p>La partie s’arrête si :</p>
<ul>
<li>tous les câbles sont révélés : les gentils gagnent</li>
<li>BOOM est révélée : les méchants gagnent</li>
<li>à la fin des 4 manches aucune des deux autres conditions n’est atteinte : les méchants gagnent</li>
</ul>
<br/><h2 id="6-design-cartes">6. Design des cartes</h2><br/>
<img src={verso} alt="face cachée"/>
<p>Une carte face cachée</p>
<img src={vide} alt="vide"/>
<p>Une carte vide</p>
<img src={cable} alt="câble"/>
<p>Une carte câble</p>
<img src={boom} alt="BOOM"/>
<p>La carte BOOM</p>
<br/>

    </div>
    )
}

export default Rules
	