import React from "react";

function Gentil(props) {
    return <div id="roleGentil">
        Tu es un gentil. Essaie de retrouver tous les câbles pour désamorcer la bombe !
    </div>
}

function Mechant(props) {
    return <div id="roleMechant">
        Tu es un terroriste. Empêche les gentils de retrouver les câbles et essaie d’activer la bombe.
    </div>
}

function Role (props) {
    const isGentil = props.role;
    if (isGentil === "gentil") { 
        return <Gentil />;
    } else {
        return <Mechant />;
    }
}

export default Role