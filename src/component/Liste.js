import React from "react";

function Liste (props) {
    return(
    <ul>{props.players.map(player => <li>{player}</li>)}</ul>
    )
}

export default Liste