import React from "react";
import Card from './Card.js';

const CardsPlayer = (props) => {
    return(
        <div id= "Cards">
            {props.player.cards.map((card) => (
                <Card card = {card}  data={props.data} idPlayer={props.idPlayer} idRoom={props.idRoom} player={props.player}/>
            ))}
        </div>
    )
}

export default CardsPlayer
	