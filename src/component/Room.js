import React, {useState} from "react";



function Room({onRoomChange, setIdRoom, setIdPlayer, goToRules}) {

  const [pseudo, setPseudo] = useState("")
  const [room, setRoom] = useState(0)

  function createroom () {
    fetch("http://176.149.223.103/creer-salon", {
      method: 'post',
      body: JSON.stringify({"pseudo":pseudo})
  }
  ).then((resp) => resp.json())
  .then((json) => setIdRoom(json.ID));
    if(onRoomChange!==undefined) {
        onRoomChange()
    }
    setIdPlayer(0)

  }


  function joinroom () {
    fetch("http://176.149.223.103/rejoindre-salon", {
      method: 'post',
      body: JSON.stringify({"person" : {"pseudo" : pseudo},
      "id_room" : {"ID" : room}})
  }
  ).then((resp) => resp.json())
  .then((json) => setIdPlayer(json.id));

  setIdRoom(room)

    if(onRoomChange!==undefined) {
        onRoomChange()
    }

  }

 

  return (
  
    <div id="body">

    <p>
      Bienvenue sur mon site ! J'espère que tu y passeras un bon moment. <br />
      Pour le moment on peut simplement jouer à Time Bomb mais j'espère y ajouter du contenu avec le temps. <br /> <br />
      Pour jouer c'est juste là :
    </p>

    <p>
      <input
        type="text"
        placeholder="Votre pseudo"
        onChange={event => setPseudo(event.target.value)}
      />
      <button onClick = {createroom}>Créer salon</button>
    </p>

    <p>
      <input
        type="text"
        placeholder="ID du salon à rejoindre"
        onChange={event => setRoom(event.target.value)}
      />
      <button onClick = {joinroom}>Rejoindre salon</button>
    </p>

    <p>
      <br /> Si tu connais pas les règles de Time Bomb, clique <a id="linkRules" onClick={goToRules}>ici</a>.
    </p>

    </div>
  );
}

export default Room;