import React from "react";
import vide from "../images/time bomb/carte vide.png";
import cable from "../images/time bomb/cable.png";
import boom from "../images/time bomb/boom.png";
import verso from "../images/time bomb/face cachée.png";

function flipCard (idRoom, idPlayer, num) {
    fetch("http://176.149.223.103/flip-card", {
      method: 'post',
      body: JSON.stringify({"id_room" : {"ID" : idRoom},
      "id_player" : {"ID" : idPlayer},
      "num_card" : {"number" : num}
      })});
  }

function Card (props) {
    const type = props.card.type;
    const face = props.card.face;
    const num = props.card.num;
    const idRoom = props.idRoom;
    const idPlayer = props.idPlayer;
    const currentPlayerID = props.data.currentPlayerID;
    const idPlayerCard = props.player.ID
    const allReady = props.data.allReady

    if (face === "verso") {
        if (idPlayer === currentPlayerID && idPlayer !== idPlayerCard && allReady === true ) {
            return <img src={verso} onClick={() => flipCard(idRoom, idPlayerCard, num)} alt="home" height="100px" />;
        } 
        return <img src={verso} alt="home" height="100px" />;
    } else {
        if (type === "vide") {
            return <img src={vide} alt="home" height="100px" />;
        } else if (type === "cable") {
            return <img src={cable} alt="home" height="100px" />;
        } else if (type === "boom") {
            return <img src={boom} alt="home" height="100px" />;
        } 
    }
}

export default Card