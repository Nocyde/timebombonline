import React from "react";

const Footer = (props) => {
    return(
        <footer>
		<p>
			Un site de Quentin Nicod - Copyright &copy; 2021 - Tous droits réservés - 
			code source libre d'accès : <a href="https://gitlab.com/Nocyde/timebombonline" target="_blank">site</a> et <a href="https://gitlab.com/Nocyde/serveur" target="_blank">serveur</a>
		</p>
	</footer>
    )
}

export default Footer
	