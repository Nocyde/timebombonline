import React from "react";

function PseudoPlayerNotCurrent(props) {
    return <div id="pseudoPlayerNotCurrent">
        {props.user.pseudo}
    </div>
}

function PseudoCurrent(props) {
    return <div id="pseudoCurrent">
        {props.user.pseudo}
    </div>
}

function PseudoPlayer (props) {
    const currentPlayer = props.data.currentPlayer
    if (currentPlayer === props.player.pseudo) {
        return <PseudoCurrent user={props.player}/>;
    } else {
        return <PseudoPlayerNotCurrent user={props.player}/>;
    }
}

export default PseudoPlayer