import React from "react";

function PseudoGentil(props) {
    return (<div id="pseudoGentil">
        {props.user.pseudo}
    </div>)
}

function PseudoMechant(props) {
    return <div id="pseudoMechant">
        {props.user.pseudo}
    </div>
}

function PseudoCurrent(props) {
    return <div id="pseudoCurrent">
        {props.user.pseudo}
    </div>
}

function Pseudo (props) {
    const isGentil = props.user.role;
    const currentPlayer = props.game.currentPlayer
    if (currentPlayer === props.user.pseudo) {
        return <PseudoCurrent user={props.user}/>;
    } else if (isGentil === "gentil") { 
            return <PseudoGentil user={props.user}/>;
        } else {
            return <PseudoMechant user={props.user}/>;
        }
}

export default Pseudo