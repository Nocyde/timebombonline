import React from "react";
import CardsPlayer from './CardsPlayers.js';
import PseudoPlayer from './PseudoPlayer.js'
import IsReady from "./IsReady.js"

const PseudoCardsPlayers = (props) => {
    return(
        <div id= "PseudoCardsPlayers">
            {props.data.players.map((player) => (
                <div id = "PseudoCardsPlayers">
                    <PseudoPlayer player = {player} data = {props.data}/>
                    <IsReady player = {player}/>
                    <CardsPlayer player = {player} data = {props.data} idPlayer={props.idPlayer} idRoom={props.idRoom}/>
                </div>
            ))}
        </div>
    )
}

export default PseudoCardsPlayers
	