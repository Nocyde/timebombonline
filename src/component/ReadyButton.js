import React from "react";

function imready (idRoom, idPlayer) {
    fetch("http://176.149.223.103/ready", {
      method: 'post',
      body: JSON.stringify({"id_room" : {"ID" : idRoom},
      "id_player" : {"ID" : idPlayer},
      })})
}

function ReadyButton (props) {
    const ready = props.user.ready;
    if (ready === false) {
        return <button onClick = {() => imready(props.idRoom, props.idPlayer)}>READY</button>;
    } else {
            return <p></p>;
        }
}

export default ReadyButton
	