import React from "react";
import Game from "./Game";
import Room from "./Room";
import Rules from "./Rules";

function Router (props) {
    const [pageCourante, setPageCourante] = React.useState("room");
    const [idRoom, setIdRoom] = React.useState(1);
    const [idPlayer, setIdPlayer] = React.useState(0);

    if (pageCourante === "room") {
        return(<Room onRoomChange = {() => setPageCourante("game")} setIdRoom = {setIdRoom} setIdPlayer = {setIdPlayer} goToRules = {() => setPageCourante("rules")}/>)
    }
    if (pageCourante === "game") {
        return(<Game idRoom = {idRoom} idPlayer = {idPlayer}/>)
    }
    if (pageCourante === "rules") {
        return(<Rules />)
    }
}

export default Router
	
