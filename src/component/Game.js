import '../styles.css';
import Role from "./Role.js"
import Avancement from './Avancement';
import Pseudo from './Pseudo';
import CardsPlayer from './CardsPlayers.js';
import PseudoCardsPlayers from './PseudoCardsPlayers.js';
import React, {useEffect} from "react";
import ReadyButton from "./ReadyButton.js";
import Liste from "./Liste.js"
import dataProcessing from '../utils/DataProcessing.js'

function Game({idRoom, idPlayer}) {

  const [data, setData] = React.useState();

  React.useEffect(() => {
    refreshState()
  }, []);

  useEffect(() => {
    const timer = setTimeout(() => {
      refreshState()
    }, 1000);
    return () => clearTimeout(timer);
  }, [data]);


  function refreshState () {
    fetch("http://176.149.223.103/get-data", {
      method: 'post',
      body: JSON.stringify({"ID": idRoom})})
      .then((resp) => resp.json())
      .then((json) => setData(json));
  }

  if (data === undefined) {
    return(<div id="body">Chargement...</div>)
  }

  function creategame () {
    fetch("http://176.149.223.103/creer-partie", {
      method: 'post',
      body: JSON.stringify({"ID": idRoom})})
    console.log("pipi")
  }

  var id_player = idPlayer;
  
  if (data.game === "non initiée") {
    if (id_player === 0 && data.players.length > 3 && data.players.length < 9) {
    return (
      <div id="body">
        Liste des joueurs présents dans le salon {idRoom} :
        <div>
          <Liste players = {data.players}/>
        </div>
        <div>
          <button onClick = {creategame}>Lancer partie</button>
        </div>
       
      </div>
    ) }
    return (
      <div id="body">
        Liste des joueurs présents dans le salon {idRoom} :
        <div>
          <Liste players = {data.players}/>
        </div>
        Parties possibles de 4 à 8 joueurs
       
      </div>
    )
  } 
  

  dataProcessing(data, id_player)


  if (data.user.ready === false) {
    for (var i = 0; i < data.user.cards.length; i++) {
        data.user.cards[i].face = "recto";
    }
  }

  
  if (data.winnersList !== "vide") {
    return (<div id="body">
    Félicitations aux vainqueurs :
    <div>
      <Liste players = {data.winnersList}/>
    </div>
   
  </div>)
  } else {

    return (
      
      <>
      <div>
        <Role role = {data?.user?.role}/> 

        <div id="tableaudejeu">

          <div class="leftbox">

            <Avancement game = {data}/>

            <div id="pseudoCardsUser">
              <Pseudo user = {data.user} game = {data}/>
              <CardsPlayer player = {data?.user} data = {data} idPlayer={idPlayer} idRoom={idRoom}/>
              <p>
                <ReadyButton user = {data.user} idRoom = {idRoom} idPlayer = {idPlayer}/>
              </p>
            </div>

          </div>

          <div class="rightbox">

            <PseudoCardsPlayers data = {data} idPlayer={idPlayer} idRoom={idRoom}/>

          </div>

        </div>

      </div>
      </>
    );
  } 
}


export default Game;
