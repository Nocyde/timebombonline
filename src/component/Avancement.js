import React from "react";

function Cable (props) {
    if (props.game.numberOfCables > 1) { 
        return (
            <p>
                Cable découvert {props.game.discoveredCables}/{props.game.numberOfPlayers}
            </p>
            )
    } else {
        return (
            <p>
                Cables découverts {props.game.discoveredCables}/{props.game.numberOfPlayers}
            </p>
        )
    } 
}

const Avancement = (props) => {
    return(
        <div id="avancement">
            <p>
                {props.game.currentPlayer} a la main
            </p>
            <p>
                Manche {props.game.currentRound}/4
            </p>
            <p>
                Tour {props.game.currentTurn}/{props.game.numberOfPlayers}
            </p>
            <p>
                <Cable game = {props.game}/>
            </p>
        </div>
    )
}

export default Avancement