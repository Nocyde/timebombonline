import React from "react";
import logo from "../images/logo fox complet.png"

const Header = (props) => {
    return(
        <header>
            <title>Impish Fox</title>
            <ul>
                <li>
                    <a href="">
                        <img src={logo} alt="home" height="100px" />
                    </a>
                </li>
            </ul>
        </header>
    )
}

export default Header