# Time Bomb 

## 1. Avant la partie :

Pour lancer une partie, l'un des joueurs doit créer un salon. Il doit ainsi simplement renseigner son pseudo et cliquer sur le bouton Créer un salon.
Il indique ensuite aux autres joueurs le numéro de son salon pour qu'ils puissent le rejoindre. Ils doivent également renseigner leur pseudo mais aussi le numéro du salon qu'ils souhaitent rejoindre avant de cliquer sur le bouton Rejoindre un salon.

Un salon peut accueillir jusqu’à 8 joueurs maximum, et on ne peut lancer la partie que s’il y en a 4 minimum.
Aucun joueur ne peut rejoindre un salon quand une partie y est déjà en cours.

## 2. But du jeu

C'est un jeu un tout petit peu manichéen. Il y a les gentils contre les méchants.

Au début de la partie vous découvrez dans quel camp vous êtes mais ne connaissez pas le rôle des autres joueurs, à vous de tenter d'identifier vos partenaires !

Les gentils gagnent s'ils désamorcent la bombe, les méchants gagnent si elle explose.

## 3. Mise en place :

Deck cartes Rôle :

| Nombre de joueurs | Gentils | Méchants |
| --- | --- | --- |
| 4 | 3 | 2 |
| 5 | 3 | 2 |
| 6 | 4 | 2 |
| 7 | 5 | 3 |
| 8 | 5 | 3 |

Distribuer une carte par joueur puis jeter ce qu’il reste (il y a une carte en trop dans les cas 4 et 7 joueurs)

Deck cartes Câble :

| Nombre de joueurs | Cartes vides | Câbles | BOOM |
| --- | --- | --- | --- |
| 4 | 15 | 4 | 1 |
| 5 | 19 | 5 | 1 |
| 6 | 23 | 6 | 1 |
| 7 | 27 | 7 | 1 |
| 8 | 31 | 8 | 1 |

Il y a toujours exactement une carte BOOM, une carte câble par joueur et on complète par des cartes vides afin qu'il y ait 5 cartes par joueur.

Distribuer 5 cartes à chaque joueur.

Choisir un joueur aléatoirement qui aura la main en début de partie.

Chaque joueur prend connaissance de ses cartes, il est en "En attente". Chacun voit alors ses propres cartes mais pas celles des autres. Quand on est prêt, on peut appuyer sur le bouton READY. Nos cartes se mélangent alors avant d'être retournées. Quand plus personne n'est en attente, la partie commence. 

## 4. Cœur de la partie

On enchaîne 4 manches :

Déroulement d’une manche :

- le joueur qui a la main peut cliquer sur n’importe quelle carte sauf l’une des siennes pour la retourner
- la carte est dévoilée pour tout le monde
- le joueur dont la carte vient d’être choisie a à présent la main et peut cliquer sur une carte
- lorsqu’autant de cartes qu’il y a de joueurs ont été dévoilées, la manche est finie

## 5. Fin de partie

La partie s’arrête si :

- Tous les câblessont révélés : les gentils gagnent
- BOOM est révélée : les méchants gagnent
- A la fin des 4 manches aucune des deux autres conditions n’est atteinte : les méchants gagnent

## 6. Design cartes

![face cachée](./images/time bomb/face cachée.png)

Une carte face cachée

![vide](./images/time bomb/carte vide.png)

Une carte vide

![câble](./images/time bomb/cable.png)

Une carte câble

![BOOM](./images/time bomb/boom.png)

La carte BOOM

