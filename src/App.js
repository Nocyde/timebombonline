import React from "react";
import Router from "./component/Router";
import Footer from "./component/Footer";
import Header from "./component/Header";

function App() {
   
  return (
  <div>
    <Header />

    <Router />

    <Footer />
  </div>
  ) 
}


export default App;
