


//traitement des données reçues par le serveur pour les mettre dans une forme plus
//simplement lisible pour le front end (le user est séparé des autres players)

function dataProcessing (data, id_player) {
    

    for (var i = 0; i < data.players.length; i++) {
        if (data.players[i].ID === data.currentPlayer) {
            data.currentPlayerID = data.currentPlayer;
            data.currentPlayer = data.players[i].pseudo;
            break;
        }
      }

      for (var x = 0; x < data.players.length; x++) {
          for (var y = 0; y < data.players[x].cards.length; y++) {
            data.players[x].cards[y].num = y
          }
      }
    
      for (var j = 0; j < data.players.length; j++) {
          if (data.players[j].ID === id_player) {
              data.user = data.players[j];
              break;
          }
      }

      data.allReady = true

      for (var z = 0; z < data.players.length; z++) {
        if (data.players[z].ready === false) {
            data.allReady = false
        }
      }
     
      data.players = data.players.filter(player => player.ID !== id_player)

      return data

}






export default dataProcessing
