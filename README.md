# Time Bomb Online
Projet visant à permettre de jouer à Time Bomb en ligne entre amis

## 1. Avant la partie :

Les joueurs pourront créer un salon. Ensuite deux options :

- Le salon possède un code que les autres joueurs peuvent utiliser pour le rejoindre (à la Among Us)
- Cela indique un lien au créateur du salon qu’il peut transmettre à ses amis

Comme il n’y a pas d’inscription sur le site pour le moment, les joueurs doivent choisir un pseudo lorsqu’ils créent/rejoignent un salon. (plus éventuellement un avatar si je suis motivé) (pourquoi pas enregistrer le pseudo dans un cookie)

Un salon peut accueillir jusqu’à 8 joueurs maximum, et on ne peut lancer la partie que s’il y en a 4 minimum

## 2. Initialisation de la partie :

Deck cartes Rôle :

| Nombre de joueurs | Gentils | Méchants |
| --- | --- | --- |
| 4 | 3 | 2 |
| 5 | 3 | 2 |
| 6 | 4 | 2 |
| 7 | 5 | 3 |
| 8 | 5 | 3 |

Distribuer une carte par joueur puis jeter ce qu’il reste (il y a une carte en trop dans les cas 4 et 7 joueurs)

Deck cartes Câble :

| Nombre de joueurs | Cartes vides | Câbles | BOOM |
| --- | --- | --- | --- |
| 4 | 15 | 4 | 1 |
| 5 | 19 | 5 | 1 |
| 6 | 23 | 6 | 1 |
| 7 | 27 | 7 | 1 |
| 8 | 31 | 8 | 1 |

Distribuer 5 cartes par joueur.

Choisir un joueur aléatoirement qui aura la main en début de partie.

## 3. Cœur de la partie

On enchaîne 4 manches :

Déroulement d’une manche :

- on distribue 5-4-3-2 cartes à chaque joueur (respectivement aux manches 1-2-3-4)
- les joueurs voient leurs propres cartes puis celles-ci sont mélangées et retournées face cachées
- le joueur qui a la main peut cliquer sur n’importe quelle carte sauf l’une des siennes pour la retourner
- la carte est dévoilée pour tout le monde
- le joueur dont la carte vient d’être choisie a à présent la main et peut cliquer sur une carte
- lorsqu’autant de cartes qu’il y a de joueurs ont été dévoilées, la manche est finie

## 4. Fin de partie

La partie s’arrête si :

- Tous les câblessont révélés : les gentils gagnent
- BOOM est révélée : les méchants gagnent
- A la fin des 4 manches aucune des deux autres conditions n’est atteinte : les méchants gagnent

## 5. Interface

But : rendre lisibles les informations suivantes :

- quel rôle nous jouons
- qui a la main
- à quelle manche nous sommes
- combien de tours reste-t-il dans la manche actuelle
- combien de câbles reste-t-il à trouver pour que les gentils gagnent
- toutes les cartes qu’il reste en jeu et à qui elles appartiennent
- timer depuis le début de la partie ?

![interface](./images/time bomb/interface.jpg)

Le component Role affiche une phase : si gentil : &quot;Tu es un gentil. Essaie de retrouver tous les câbles pour désamorcer la bombe !&quot; (en bleu)
 si méchant : &quot;Tu es un terroriste. Empêche les gentils de retrouver les câbles et essaie d’activer la bombe. !&quot; (en rouge)

Manche _currentRound_ / 4

Tour _currentTurn_ / _numberOfPlayers_

Câble(s) découvert(s) _discoveredCables_ / _numberOfPlayers_

Le _pseudo_ du joueur est également rouge ou bleu selon son rôle.

Le pseudo du joueur qui a la main est orange.

Le pseudo des autres joueurs est écrit j’ai mis J1, J2, ect pour l’exemple.

## 6. Design cartes

![face cachée](./images/time bomb/face cachée.png)

Une carte face cachée

![vide](./images/time bomb/carte vide.png)

Une carte vide

![câble](./images/time bomb/cable.png)

Une carte câble

![BOOM](./images/time bomb/boom.png)

La carte BOOM

